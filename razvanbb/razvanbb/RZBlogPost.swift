//
//  RZBlogPost.swift
//  razvanbb
//
//  Created by Virgil Adumitroae on 13/02/2017.
//  Copyright © 2017 Virgil Adumitroae. All rights reserved.
//

import Foundation

class RZBlogPost {
    
    var postTitle = String()
    var postLink = String()
}
