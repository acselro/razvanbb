//
//  RZTableViewController.swift
//  razvanbb
//
//  Created by Virgil Adumitroae on 13/02/2017.
//  Copyright © 2017 Virgil Adumitroae. All rights reserved.
//

import UIKit

class RZTableViewController: UITableViewController, XMLParserDelegate {
    
    var parser = XMLParser()
    var blogPosts = [RZBlogPost]()
    var postTitle = String()
    var postLink = String()
    var eName = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: "http://razvanbb.ro/feed/")
        self.parser = XMLParser(contentsOf: url!)!
        parser.delegate = self
        parser.parse()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.blogPosts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let blogPost = self.blogPosts[indexPath.row]
        cell.textLabel?.text = blogPost.postTitle
        return cell
    }
    
    // MARK: - XMLParserDelegate methods
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        self.eName = elementName
        
        if elementName == "item" {
            self.postTitle = String()
            self.postLink = String()
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        if (!data.isEmpty) {
            if self.eName == "title" {
                self.postTitle += data
            } else if self.eName == "link" {
                self.postLink += data
            }
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "item" {
            let blogPost = RZBlogPost()
            blogPost.postTitle = self.postTitle
            blogPost.postLink = self.postLink
            self.blogPosts.append(blogPost)
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "viewPost" {
            let blogPost = self.blogPosts[(self.tableView.indexPathForSelectedRow?.row)!]
            let viewController = segue.destination as! RZPostViewController
            viewController.postLink = blogPost.postLink
        }
    }

}
