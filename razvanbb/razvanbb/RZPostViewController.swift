//
//  RZPostViewController.swift
//  razvanbb
//
//  Created by Virgil Adumitroae on 13/02/2017.
//  Copyright © 2017 Virgil Adumitroae. All rights reserved.
//

import UIKit

class RZPostViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    var postLink = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: self.postLink)
        let request = URLRequest(url: url!)
        self.webView.loadRequest(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
